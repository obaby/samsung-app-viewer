#Samsung APP Viewer

---------------------
###文件格式
【一】前面10个字节分为两段：
     [][][][][][]：APPL11 或 APPL10 表示APP文件的版本 
     [][][][]    ：DWORD类型数据，文件头长度：APP信息+PNG文件 

 【二】然后开始的就是TV（Tag Value）格式的数据段列表，
     Tag     ：为4字节大写字母，表示数据段名称 
     Value格式如下： 
         char  ：1字节，看样子只有0/1两个值，因此推测其实是bool值 
         short ：2字节，或为 unsigned short 
         DWORD ：4字节，或为 int，unsigned int 
         DATE  ：14字节，表示时间，格式为 YYYYMMDDHHmmSS 
         STRING：至少2字节，长度可变，前面两字节是一个short类型表示字符串长度，后面是字符串内容并多为unicode编码 
         BINARY：至少4字节，长度可变，前面死字节是一个DWORD类型表示二进制数据长度，后面是二进制数据内容（例如img图片） 

     TV数据段详细列表：
     序号      Tag         类型          注释 
     0         "TITL"      STRING(u)     标题 
     1         "VERS"      STRING        版本 
     2         "LCID"      DWORD         未知 
     3         "AGEL"      CHAR          未知 
     4         "SPRC"      STRING        价格 
     5         "NPRC"      STRING        价格（不知道为什么有两个价格，数据还都相同） 
     6         "CTGR"      STRING        类别 
     7         "RATE"      char          未知 
     8         "RDAT"      DATE          未知 
     9         "ODAT"      DATE          未知 
     10        "DDAT"      DATE          下载时间 
     11        "IDAT"      DATE          安装时间 
     12        "PLFM"      STRING(u)     类型，"Native_App" 或 "Native_Widget" 
     13        "PDID"      STRING(u)     编号 
     14        "USID"      STRING(u)     购买者 
     15        "SLID"      STRING(u)     开发者 
     16        "SPID"      STRING(u)     *未知 
     17        "IURL"      STRING(u)     未知 
     18        "ICPL"      char          安装标志 
     19        "IMEI"      STRING(u)     手机串号 
     20        "MLST"      short         未知 
     21        "MODL"      STRING(u)     机型，"GT-S8500" 
     22        "THNL"      BINARY        缩略图 
     23        "PACK"      BINARY        应用程序打包数据，免费或可XX的打包数据是一个ZIP文件（以PK开始）

###程序截图
![img](https://bitbucket.org/obaby/samsung-app-viewer/raw/3b8b297a4a3cb9b278ea8405d7b355b35b77e6b0/screenshot.png)

------------------
>火星信息安全研究院
>
>http://www.h4ck.org.cn


